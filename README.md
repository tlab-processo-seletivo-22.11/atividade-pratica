**<h2>Respostas das questões teóricas</h2>**

<ol>
    <li>Git é uma ferramenta desenvolvida para facilitar o processo de criar e modificar códigos, sozinho ou em grupo, de forma que nenhuma das etapas dos códigos sejam perdidas, ou seja, um código não sobrescreve o outro. Também facilita o compartilhamento de códigos com várias pessoas.</li>
    <li>Na staging area encontram-se os arquivos com mudanças que ainda serão incluídas no próximo commit.</li>
    <li>Local onde ficam todos os arquivos que estão sendo trabalhados.</li>
    <li>É a aplicação das alterações que estavam na staging area, significa que aquelas mudanças agora farão parte do código.</li>
    <li>São ramificações dos commits. Existe a branch master que é a principal, as outras branches criadas são importantes para não misturar um código instável com o resto do projeto.</li>
    <li>Head é a branch atual e vai mudando conforme se acessa outra branch.</li>
    <li>Ocorre quando se junta os commits de diferentes branches.</li>
    <li> Untracked: arquivos recém-adicionados;<br> 
    Unmodified: arquivos inalterados desde o último commit; <br>
    Modified: modificados desde o último commit;<br>
    Staged: preparados para comitar. </li>
    <li>Git init cria um repositório.</li>
    <li>Git add envia o arquivo para a staging area.</li>
    <li>Git status verifica se o arquivo foi, de fato, enviado.</li>
    <li>Faz com que o arquivo, que está na staging area, seja versionado.</li>
    <li>Git log permite ver o histórico de commits feitos.</li>
    <li>Git checkout -b cria ramos a partir do master.</li>
    <li>O git reset é usado para restaurar versões anteriores do código e existe de 3 formas: soft, mixed e hard. A soft move o head para o commit indicado, mas não altera o resto. Mixed permite mover o head, alterar o staging e não muda o working directory. Por fim, a hard volta para um commit anterior e altera o staging e o working directory em definitivo.</li>
    <li>O git revert permite criar um novo commit com as mudanças do commit indicado.</li>
    <li>Git clone baixa o respositório na máquina.</li>
    <li>Git push é usado para subir as alterações para o repositório remoto no Gitlab.</li>
    <li>Git pull é usado para buscar e baixar na máquina conteúdo de repositórios remotos.</li>
    <li>Para não realizar um commit com determinado(s) arquivo(s), é necessário criar o arquivo .gitignore onde serão colocados todos os arquivos que se deseja ignorar.</li>
    <li>Master é o ramo principal, é a origem do projeto. A partir do master, cria-se o ramo develop, é nele que serão colocadas as alterações para correção/evolução de funções já existentes, como também o desenvolvimento de novas funções. O staging é outro ramo, mas criado a partir do develop, e é onde são feitos os testes com as modificações de código no develop. Depois, se essas alterações forem aprovadas em todos os testes, o staging é mesclado ao master, gerando uma nova versão do software.</li>

<ol>